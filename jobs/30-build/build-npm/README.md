## Objective

Build a npm based project


## How to use it

1. Create a package.json
2. create a build target in your package.json
6. Well done, your job is ready to work ! 😀

## Job details

* Job name: `npm-build`
* Docker image: node16-alpine
* Default stage: `build`

### Build behavior

the job is executed with the $RUNNER_TAG tag
it store in the artifacts pane the dist folder.

## Author
SLongo@klanik.com
