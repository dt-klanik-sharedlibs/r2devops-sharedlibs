## Objective

Build a docker image based project


## How to use it

1. Create a Dockerfile
6. Well done, your job is ready to work ! 😀

## Job details

* Job name: `build-docker`
* build image : docker:19.03.12
* Default stage: `build`

### Build behavior

the job is executed with the $RUNNER_TAG tag
it store in the artifacts pane the dist folder.

## Author
slongo@klanik.com
