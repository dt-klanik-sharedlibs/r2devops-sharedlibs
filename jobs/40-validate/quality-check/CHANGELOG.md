# Changelog
All notable changes to this job will be documented in this file.

## [0.2.4] - 2022-12-08
* fix merge request scripts

## [0.2.3] - 2022-12-08
* fix merge request scripts

## [0.2.2] - 2022-12-08
* remove unit test dependency

## [0.2.1] - 2022-12-08
* change unit test job name

## [0.2.0] - 2022-12-08
* change unit test job name

## [0.1.0] - 2022-12-08
* Initial version
